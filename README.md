# RC4Git
The easiest way to collaborate over any Github / Gitlab hosted projects.

## Instant community empowerment!

One single click directly from your code repository, and your community can instantly collaborate with rich group chat, exchange ideas and code, start voice or video conferences.  The project's Github / Gitlab activities are displayed in streaming real time and visible to all your community members.

RC4GIT can be setup in minutes for your Github or Gitlab respository, and is designed from day one to grow with your community, easily handling thousands of community members per project.
